import {lazy} from "react"
import address_children from "./children/address"
import discover_children from "./children/discover"
import mine_children from "./children/mine"
import weixin_children from "./children/weixin"

const routes=[
    {
        path:"/login",
        name:"login",
        icon:"",
        isshow:false,
        element:lazy(()=>import("../pages/login")),
    }, {
        path:"/register",
        name:"register",
        icon:"",
        isshow:false,
        element:lazy(()=>import("../pages/register")),
    },
    {
        path:"/chat",
        name:"chat",
        icon:"",
        isshow:false,
        element:lazy(()=>import("../pages/chat")),
    },
    {
        path:"/main/*",
        element:lazy(()=>import("../pages/main")),
        name:"main",
        icon:"",
        isshow:false,
        children:[
            {
                path:"weixin/*",
                name:"微信",
                icon:"iconfont icon-weixinxiaoxixianxing",
                icons:"clickicon iconfont icon-xiaoxi-2",
                isshow:true,
                element:lazy(()=>import("../pages/main/weixin")),
                children:weixin_children
            },{
                path:"address/*",
                name:"通讯录",
                icon:"iconfont icon-tongxunlu1",
                icons:"clickicon iconfont icon-tongxunlu_tongxunlu",
                isshow:true,
                element:lazy(()=>import("../pages/main/address")),
                children:address_children
            },{
                path:"discover/*",
                name:"发现",
                icon:"iconfont icon-faxian",
                icons:"clickicon iconfont icon-20",
                isshow:true,
                element:lazy(()=>import("../pages/main/discover")),
                children:discover_children
            },{
                path:"mine/*",
                name:"我的",
                icon:"iconfont icon-wode",
                icons:"clickicon iconfont icon-wode-active",
                isshow:true,
                element:lazy(()=>import("../pages/main/mine")),
                children:mine_children
            },
        ]
    },{
        path:"*",
        to:"/login"
    }
]

export default routes