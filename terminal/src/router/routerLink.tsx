import { useNavigate } from "react-router"
import withRouter from "./withRouter"
import './routerLink.less'

function RouterLink(props:any) {
   const navigate= useNavigate()
    return (
        <div className="changeNavs">
            {
                props.nav.map((el:any, i:any)=>{
                    return <div className="changeNav" onClick={()=>{navigate({pathname:el.path})}} key={el.path}>
                        <div className={props.location.pathname === '/main/'+el.path ? 'clickp' : ''}>
                            <i className={el.icon}></i>
                        </div>
                        <p className={props.location.pathname === '/main/'+el.path ? 'clickp' : ''}>{el.name}</p>
                    </div>
                })
            }
        </div>
    )
}

export default withRouter(RouterLink)