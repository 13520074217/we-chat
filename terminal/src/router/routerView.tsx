import { Suspense } from "react"
import { Navigate, Route, Routes} from "react-router"
function RouterView(props:any) {
    return (
        <>
            <Routes>
                {
                    props.routes.map((el:any,i:any)=>{
                        //有子路由
                        if(el.children){
                            return <Route path={el.path} element={<Suspense fallback={"加载中"}>
                                <el.element routes={el.children}></el.element>
                            </Suspense>} key={i}></Route>
                        }else if( el.name){
                            //没有子路由
                            return <Route path={el.path} element={<Suspense fallback={"加载中"}>
                                <el.element></el.element>
                            </Suspense>}  key={i} ></Route>
                        }else if(!el.name){
                            //重定向
                            return <Route path={el.path} element={<Navigate to={el.to}></Navigate>}  key={i} ></Route>
                        }
                    })
                }
            </Routes>
        </>
    )
}

export default RouterView