import request from "./index"

export function getList(url:any, params:any) {
    return request({
        url,
        method: "GET",
        params
    })
}
export function postList(url:any, data:any) {
    return request({
        url,
        method: "POST",
        data
    })
}