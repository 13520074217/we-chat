export const terminal = ():string => {//判断终端类型
  let agents:string[] = ['Android', 'iPhone', 'SymbianOS', 'Windows Phone','iPad'];
  let result:string = "";
  for(let i:number = 0; i <agents.length; i++){
    if ((navigator as Navigator).userAgent.indexOf(agents[i]) > -1) {
      result = agents[i];
      break;
    }
  }
  return result
}
