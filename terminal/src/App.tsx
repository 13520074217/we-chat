import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import './App.less';
import RouterView from './router/routerView';
import routes from './router/routes';
import store from './store';

function App() {
  return (
    <Provider store={store}>
      <div className="App">
      <BrowserRouter>
        <RouterView routes={routes}></RouterView>
      </BrowserRouter>
    </div>
    </Provider>
  );
}

export default App;
