import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {terminal} from '../../utils/public';//返回终端类型
import style from './search.module.less';
import SearchBox from './search-box';
let openSearchDom:HTMLDivElement | null;//弹出层dom节点
const initOutsiteBox = ():HTMLDivElement=>{//单例创建弹出层dom节点
  if(!openSearchDom){
    openSearchDom = document.createElement('div');
    openSearchDom.className = style['search-box'];
  };
  return openSearchDom;
};
const openSearch = () => {//打开弹出层方法
  let openSearchDom:HTMLDivElement = initOutsiteBox();
  ReactDOM.render(<SearchBox close={closeSearch}  />, openSearchDom);
  document.body.appendChild(openSearchDom);
};
const closeSearch = () => {//关闭弹出层方法
  document.body.removeChild(openSearchDom as HTMLDivElement);
  openSearchDom = null;
}
const Search:React.FC = () => {
  let terminalItem:string = terminal();
  if (terminalItem === 'iPhone' || terminalItem === 'iPad' ) {
    return (
      <div className={style["search-IOS"]} onClick={openSearch}>&#xe612; 搜索</div>
    );
  } else {
    return (
      <div>安卓或其他</div>
    );
  };
};
export default Search;
