import * as React from 'react';
import style from  './search.module.less';
import HeaderBar from '../header-bar';
interface SearchTitle {
  close?:()=>void;
};
const SearchBox:React.FC<SearchTitle> = props => {
  return (
    <div className={style['search-frame']}>
      <HeaderBar title="许儒家是猪" icon="back" more={props.close}>
      </HeaderBar>
    </div>
  );
};
export default SearchBox;
