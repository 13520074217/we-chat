import * as React from 'react';
import style from './header-bar.module.less';
interface Style {
  [propName: string]: string;
}
interface Header {//HeaderBar所需要的props属性
  title?:string;//标题
  style?:Style;//外框额外样式设置
  icon?:string;//icon按钮图标   back ｜ close,
  more?:()=>void;//icon按钮所触发的方法,前提必须有icon
};
const HeaderBar:React.FC<Header> = props => {
  const left = ['back','close'].find(item=>item === props.icon);//是否显示左侧内容
  return (
    <div className={style["header-bar"]} style={props.style}>
      <div className={style["header-title"]}>
        <div className={style["left-btn"]}>
          { left && (<div className={style["icon-"+left]} onClick={props.more}></div>)}
        </div>
        <div className={style["title"]}>{props.title}</div>
        <div className={style["right-more"]}>{props.children}</div>
      </div>
    </div>
  );
};
export default HeaderBar;
