import React from 'react'
import "./userList.less"
import { useNavigate } from "react-router"
import withRouter from "../../router/withRouter"
function UserList(props:any) {
  let navigate=useNavigate();
  let { img, time, title, content, id } = props.renderData
  return (
    <div className='user-list' onClick={()=>{
      navigate({pathname:'/chat'})
    }}>
      <div className='head-img'><img src={img} alt="" /></div>
      <div className='user-box'>
        <div className='user-con'>
          <div>{title}</div>
          <p>{content}</p>
        </div>
        <div className='mes-time'>11:19</div>
      </div>

    </div>
  )
}
export default withRouter(UserList)