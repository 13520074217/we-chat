import * as React from 'react';
import style from './button.module.less';
interface Style {
  [propName: string]: string;
}
interface ButtonConfig {
  type?:string;//按钮组件类型 default|primary|dashed|text|link
  shape?:string;//是否圆角 round（圆角）|circle（圆形）
  danger?:boolean;//危险按钮(红色)
  disabled?:boolean;//是否禁用
  style?:Style;//额外样式设置
  //icon?:string;//后期是否需要用图标
};
const Button:React.FC<ButtonConfig> = props => {
  const type:string = ['default','primary','dashed','text','link'].find(item => item === props.type) || 'default';//按钮类型设置，主要为样式设置
  const shape:string | undefined = ['circle','round'].find(item => item === props.shape);//按钮形状设置
  return <button
  className={
    style['button'] + ' ' +
    style[type] +
    (shape?(' ' + style[shape]):'') +
    (props.danger?(' ' + style['danger']):'')
  }
  disabled={props.disabled}
  style={props.style}
  >{props.children}</button>
};
export default Button;
