import action_type from "../actionType"

const register_action={
    getlist(payload:any){
        return {
            type:action_type.register,
            payload
        }
    },
}

export default register_action