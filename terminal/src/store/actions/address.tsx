import action_type from "../actionType"

const address_action={
    getlist(payload:any){
        return {
            type:action_type.address,
            payload
        }
    },
}

export default address_action