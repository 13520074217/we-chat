import action_type from "../actionType"

const login_action={
    getlist(payload:any){
        return {
            type:action_type.login,
            payload
        }
    },
}

export default login_action