import action_type from "../actionType"

const weixin_action={
    getlist(payload:any){
        return {
            type:action_type.weixin,
            payload
        }
    },
}

export default weixin_action