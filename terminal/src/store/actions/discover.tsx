import action_type from "../actionType"

const discover_action={
    getlist(payload:any){
        return {
            type:action_type.discover,
            payload
        }
    },
}

export default discover_action