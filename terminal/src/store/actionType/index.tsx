import login_type from "./login";
import register_type from "./register";
import weixin_type from "./weixin";
import address_type from "./address";
import discover_type from "./discover";
import mine_type from "./mine";


const action_type= {
    login: login_type,
    register: register_type,
    weixin: weixin_type,
    address: address_type,
    discover: discover_type,
    mine:mine_type
}


export default  action_type