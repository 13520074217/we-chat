import * as React from 'react';
import { useEffect } from "react";
import { connect } from "react-redux"
import { useNavigate } from "react-router"
import action from "../../store/actions"


interface People {
    name:string;
    children?:People[]
}
const Login:React.FC<People> = props => {
    let navigate=useNavigate();
    let person:People = {
        name:'许儒家',
        children:[
            {
                name:'登录'
            }
        ]
    }
    let jump = () => {
        navigate({
            pathname:"/main/weixin/*"
        })
    }
    return (
        <div onClick={() => {jump()}}>{(person.children as People[])[0].name}</div>
    )
}


export default connect(state=>state,action)(Login)