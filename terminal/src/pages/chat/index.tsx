import React ,{useState}from 'react'
import HeaderBar from "../../components/header-bar"
import "./chat.less"
import "emoji-mart/css/emoji-mart.css"
import { Picker } from "emoji-mart"
interface Style {
    [propName: string]: string;
  }
const Chat: React.FC =()=>{
    const style: Style = {
        backgroundColor: '#fff',
        borderBottom: '1px solid #f5f5f5'
      }
      const close = (): void => {
        console.log('测试')
      }
    let [emojiShow,setEmoji]=useState(false)
    let [record,setRecord]=useState(true)
  
    return (
        <div>
        <HeaderBar title="许儒家是猪" icon="back" style={style} more={close}>
        <button>按钮</button>
      </HeaderBar>    
            <div className={emojiShow?'key-board show-emoji':'key-board hide-emoji'}>
                {
                    record?<span className='iconfont icon-131keyboard voice' onClick={()=>{
                        record=!record
                        setRecord(record)
                      
                    }}></span>:<span className='iconfont icon-yuyin voice' onClick={()=>{
                        record=!record
                        setRecord(record)
                        emojiShow=false
                        setEmoji(emojiShow)
                    }}></span>
                }
                {
                    record?<div className='record-say'>按住说话</div>:<div className='ent-tex'><textarea name='value'></textarea></div>
                }
                <div className='key-tool'>   
                 <span className={emojiShow?'iconfont icon-131keyboard':'iconfont icon-biaoqing'} onClick={()=>{
                     emojiShow=!emojiShow
                     setEmoji(emojiShow)
                 }}></span>
                <span className='iconfont icon-tianjia'></span>
                </div>
            </div>
            <Picker style={{ width: "100%", fontSize: '30', height: '270px', bottom:emojiShow?'0%':'-38%', position: 'fixed' }} emoji='' onClick={(emoji, event) => {
            }}></Picker>
        </div>
    )
}

export default Chat