import { useEffect } from "react";
import HeaderBar from "../../../components/header-bar"
import styles from './address.module.less'
import axios from "axios"


const Address: React.FC = (props) => {
    console.log(props);

    
    useEffect(()=>{
        axios.get("/xu/getData").then((res) => {
            console.log(res);
        })
    },[])
    return (
        <div className={styles['address-box']}>
            {/* 头部导航 */}
            <HeaderBar></HeaderBar>
            <div className={styles['address-content']}>
                通讯录
            </div>
        </div>
    )
}

export default Address
