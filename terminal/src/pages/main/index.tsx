import React from "react";
import RouterLink from "../../router/routerLink"
import RouterView from "../../router/routerView"
import style from './main-list.module.less';

interface MainList {
    routes?: MainList[]
};
const Main:React.FC<MainList> = props => {
    return (
        <div className={style["main-wrap"]}>
            <div className={style["routers-view"]} >
                <RouterView routes={props.routes}></RouterView>
            </div>
            <div className={style["routers-link"]}>
                <RouterLink nav={props.routes}></RouterLink>
            </div>
        </div>
    )
}

export default Main