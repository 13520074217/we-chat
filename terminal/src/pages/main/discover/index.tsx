import HeaderBar from "../../../components/header-bar"
import styles from './discover.module.less'
const Discover: React.FC = () => {
    return (
        <div className={styles["discover-box"]}>
            {/* 头部导航 */}
            <HeaderBar></HeaderBar>
            <div className={styles["discover-content"]}>
                发现
            </div>
        </div>
    )
}

export default Discover
