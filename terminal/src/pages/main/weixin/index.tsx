import HeaderBar from "../../../components/header-bar"
import React, { useEffect, useRef, useState } from "react";
import UserList from "../../../components/UserList/userList"
import styles from './wei-xin.module.less'
interface Style {
  [propName: string]: string;
}
const Weixin: React.FC = () => {
  const style: Style = {
    backgroundColor: '#fff',
    borderBottom: '1px solid #f5f5f5'
  }
  const close = (): void => {
    console.log('测试')
  }
  const [renderData, setRender] = useState([
    { img: '/img/1.png', time: 'Mon Mar 21 2022 19:48:27 GMT+0800 (GMT+08:00)', title: '共产主义接班人共产主义接班人共产主义接班人共产主义接班人共产主义接班人共产主义接班人共产主义接班人共产主义接班人共产主义接班人共产主义接班人共产主义接班人共产主义接班人共产主义接班人共产主义接班人共产主义接班人共产主义接班人共产主义接班人共产主义接班人共产主义接班人', content: '你好啊', id: '01' },
    { img: '/img/1.png', time: 'Mon Mar 21 2022 19:48:27 GMT+0800 (GMT+08:00)', title: '', content: '共产主义接班人共产主义接班人共产主义接班人共产主义接班人共产主义接班人共产主义接班人共产主义接班人共产主义接班人共产主义接班人共产主义接班人共产主义接班人共产主义接班人共产主义接班人共产主义接班人共产主义接班人共产主义接班人共产主义接班人共产主义接班人共产主义接班人', id: '01' },
    { img: '/img/1.png', time: 'Mon Mar 21 2022 19:48:27 GMT+0800 (GMT+08:00)', title: '', content: '你好啊', id: '01' },
    { img: '/img/1.png', time: 'Mon Mar 21 2022 19:48:27 GMT+0800 (GMT+08:00)', title: '', content: '你好啊', id: '01' },
    { img: '/img/1.png', time: 'Mon Mar 21 2022 19:48:27 GMT+0800 (GMT+08:00)', title: '共产主义接班人', content: '你好啊', id: '01' }
  ]
  )
  return (
    <div className={styles["weixin-box"]}>
      {/* 头部导航 */}
      <HeaderBar title="许儒家是猪" icon="back" style={style} more={close}>
        <button>按钮</button>
      </HeaderBar>
      <div className={styles["content"]}>
        {
          renderData.map((item, index) => {
            return <UserList renderData={item} key={index}></UserList>
          })
        }
      </div>
    </div>
  )
  }

  export default Weixin
