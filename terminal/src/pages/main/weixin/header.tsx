import * as React from 'react';
import Search from "../../../components/search";
import HeaderBar from "../../../components/header-bar";
import Button from "../../../components/button";
const Header:React.FC = () => {
  return (
    <div>
      <HeaderBar title="微信">
        <Button type="text" shape="circle" style={{fontSize:'18px'}}>&#xe605;</Button>
      </HeaderBar>
      <Search />
    </div>
  );
};
export default Header;
