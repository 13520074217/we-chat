var express = require('express')
var router = express.Router()
function query (sql) {
  return new Promise((resolve, reject) => {
    global.myConn.query(sql, function (error, results, fields) {
      if (error) {
        throw error
      } else {
        resolve(results)
      }
    })
  })
}

//获取数据
router.get('/getData', async function (req, res, next) {
    let sql = `select * from 表名`
    let data = await query(sql)
    res.send(data)
    console.log(data);
  })

module.exports = router