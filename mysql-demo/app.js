var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mysql = require('mysql');

var indexRouter = require('./routes/index');
var chenRouter = require('./routes/phone/chenchen');
var yangRouter = require('./routes/phone/yangweiyi');
var xuRouter = require('./routes/phone/xurujia');
var liRouter = require('./routes/phone/lizhiren');
var app = express();
var cors = require('cors'); 
app.use(cors()); 
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/chen', chenRouter);
app.use('/yang', yangRouter);
app.use('/xu', xuRouter);
app.use('/li', liRouter);
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

// 全局链接配置
// global.myConn = function() { 
//   // 创建连接
//   var connection = mysql.createConnection({
//     host     : ' localhost',   // ip 或 域名
//     database : 'wechat',   // 哪个数据库
//   //   port     : 3306,          // 修改对应端口号（默认3306）
//     user     : 'root',        // 数据库用户名
//     password : 'rootroot',    // 数据库密码
//   });
//   // 开始连接（只需要连接一次即可）
//   connection.connect();
//   return connection
// }

var connection = mysql.createConnection({
  host     : 'localhost',   // ip 或 域名
  database : 'wechat',   // 哪个数据库
  // port     : 3306,          // 修改对应端口号（默认3306）
  user     : 'root',        // 数据库用户名
  password : 'rootroot',    // 数据库密码
});
// 开始连接（只需要连接一次即可）
connection.connect();

global.myConn = connection;

module.exports = app;
