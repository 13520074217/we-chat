import {combineReducers} from "redux"
import address_reducer from "./address"
import discover_reducer from "./discover"
import mine_reducer from "./mine"
import weixin_reducer from "./weixin"

const reducer=combineReducers({
    weixin: weixin_reducer,
    discover: discover_reducer,
    mine: mine_reducer,
    address: address_reducer
})

export default reducer