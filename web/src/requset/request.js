import request from "./index"

export function getList(url, params) {
    return request({
        url,
        method: "GET",
        params
    })
}
export function postList(url, data) {
    return request({
        url,
        method: "POST",
        data
    })
}