import { useNavigate } from "react-router"
import withRouter from "./withRouter"
import './routerLink.less'

function RouterLink(props) {
   const navigate= useNavigate()
    return (
        <div className="changeNavs">
            {
                props.nav.map((el, i)=>{
                    // console.log(props.location.pathname, el.path);
                    return <div className="changeNav" onClick={()=>{navigate({pathname:el.path})}} key={el.path}>
                        <span className={props.location.pathname === '/main/'+el.path ? el.icons : el.icon}></span>
                        <p className={props.location.pathname === '/main/'+el.path ? 'clickp' : ''}>{el.name}</p>
                    </div>
                })
            }
        </div>
    )
}

export default withRouter(RouterLink)