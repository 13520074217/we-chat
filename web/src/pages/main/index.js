import RouterLink from "../../router/routerLink"
import RouterView from "../../router/routerView"


function Main(props) {
    return (
        <div>
            <RouterView routes={props.routes}></RouterView>
            <RouterLink nav={props.routes}></RouterLink>
        </div>

    )
}

export default Main