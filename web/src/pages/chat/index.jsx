import React from 'react'
import HeaderBar from "../../components/headerBar/headerBar"
import "./chat.less"
import "emoji-mart/css/emoji-mart.css"
import { Picker } from "emoji-mart"
function Chat() {
    return (
        <div>
            <HeaderBar></HeaderBar>
            <Picker style={{ width: "100%", fontSize: '30', height: '200px', bottom: '31.7%', position: 'fixed' }} emoji='' onClick={(emoji, event) => {
                    }}></Picker>
            <div className='key-board'>
                <span className='iconfont icon-yuyin voice'></span>
                <textarea name='value' number='500'></textarea>
                <div className='key-tool'>   
                 <span className='iconfont icon-biaoqing'></span>
                <span className='iconfont icon-tianjia'></span>
                </div>

            </div>
        </div>
    )
}

export default Chat